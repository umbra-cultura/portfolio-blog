---
title: Página Inicial
description: Umbra Cultura - Página Inicial
sections:
  - type: hero
    cta_label: Inscreva-se
    cta_placeholder: Type your email address
    cta_link: "#"
    hero_img: ./assets/images/hero-banner.png
    greeting: Olá, pessoal!
    text: I use animation as a third dimension by which to simplify experiences and
      kuiding thro each and every interaction. I’m not adding motion just to
      spruce things up, but doing it in ways that.
  - type: topics
    title: Hot Topics
    subtitle: Don't miss out on the latest news about Travel tips, Hotels review,
      Food guide...
  - type: featured
    title: Editor's picked
    subtitle: Featured and highly rated articles
  - type: tags
    title: Popular Tags
    subtitle: Most searched keywords
  - type: artigos
    title: Recent posts
    subtitle: Don't miss the latest trends
    limit: 5
---
