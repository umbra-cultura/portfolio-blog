module.exports = {
  login_for_admin: {
    en: "Login for administrator",
    "pt-BR": "Acesso para administradores",
  },
  switch_lang: {
    en: "Change language to English",
    "pt-BR": "Mudar idioma para Inglês",
  },
};
