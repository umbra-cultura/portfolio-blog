const yaml = require("js-yaml");
const MarkdownIt = require("markdown-it");
const markdownItAnchor = require("markdown-it-anchor");
const markdownItToc = require("markdown-it-table-of-contents");
const { EleventyI18nPlugin, EleventyRenderPlugin } = require("@11ty/eleventy");
const i18n = require("eleventy-plugin-i18n");
const pluginWebc = require("@11ty/eleventy-plugin-webc");
const translations = require("./src/_data/i18n");

const {
  readableDateFilter,
  markdownFilter,
  limitToFilter,
  tidyHTMLFilter,
  langFilter,
  dummyFilter,
} = require("./eleventy-helpers");

module.exports = function (eleventyConfig) {
  // ADD DELAY BEFORE RE-RUNNING
  eleventyConfig.setWatchThrottleWaitTime(1000);
  eleventyConfig.addPlugin(pluginWebc, {
    components: "src/_components/**/*.webc",
  });
  eleventyConfig.addPlugin(EleventyRenderPlugin);
  eleventyConfig.addPlugin(EleventyI18nPlugin, {
    defaultLanguage: "pt-BR",
    errorMode: "never", //"allow-fallback",
  });
  eleventyConfig.addPlugin(i18n, {
    translations,
    fallbackLocales: {
      "*": "pt-BR",
    },
  });

  // Disable automatic use of your .gitignore
  eleventyConfig.setUseGitIgnore(false);

  // Merge data instead of overriding
  eleventyConfig.setDataDeepMerge(true);

  // human readable date
  eleventyConfig.addFilter("readableDate", readableDateFilter);

  eleventyConfig.addFilter("limitTo", limitToFilter);
  eleventyConfig.addFilter("tidyHTML", tidyHTMLFilter);
  eleventyConfig.addFilter("langFilter", langFilter);
  eleventyConfig.addFilter("dummyFilter", dummyFilter);

  const markdown = new MarkdownIt({
    html: true,
  });

  eleventyConfig.setLibrary("md", markdown);
  eleventyConfig.addFilter("markdown", markdownFilter);

  eleventyConfig.addFilter("jsbundle", (code) => {
    require("fs").writeFileSync("in.js", code);
    require("esbuild").buildSync({
      entryPoints: ["in.js"],
      outfile: "out.js",
      minify: true,
      bundle: true,
    });
    const bundle = require("fs").readFileSync("out.js", "utf8");
    return bundle;
  });

  eleventyConfig.addPassthroughCopy("src/admin/config.yml");
  eleventyConfig.addPassthroughCopy("src/assets");
  return {
    dir: {
      input: "src",
      output: "public",
    },
    htmlTemplateEngine: "njk",
    markdownTemplateEngine: "njk",
  };
};
