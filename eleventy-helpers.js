/**
 * Use this file to declare eleventy
 */

const { DateTime } = require("luxon");
const MarkdownIt = require("markdown-it");

const markdown = new MarkdownIt({ html: true });

const markdownFilter = (value) => {
  if (!value) {
    value = "";
  }
  return markdown.render(value);
};

const readableDateFilter = (dateObj) =>
  DateTime.fromJSDate(dateObj, { zone: "utc-3" }).toFormat("dd LLL yyyy");

// Cut array, collection or string - {{ collections.news | limitTo(10) }}
const limitToFilter = (input, limit) => {
  if (typeof limit !== "number") {
    return input;
  }
  if (typeof input === "string") {
    if (limit >= 0) {
      return input.substring(0, limit);
    } else {
      return input.substr(limit);
    }
  }
  if (Array.isArray(input)) {
    limit = Math.min(limit, input.length);
    if (limit >= 0) {
      return input.splice(0, limit);
    } else {
      return input.splice(input.length + limit, input.length);
    }
  }

  return input;
};

const tidyHTMLFilter = (html) => {
  if (!html) {
    return "";
  }

  if (typeof DOMParser === "undefined") {
    return html;
  }

  const parser = new DOMParser();
  const doc = parser.parseFromString(html, "text/xml");
  if (doc.documentElement.querySelector("parsererror")) {
    return doc.documentElement.querySelector("parsererror").innerText;
  } else {
    return html;
  }
};

const langFilter = (collection, lang = "pt-BR") =>
  collection.filter((item) => item.data.lang == lang);

// Use it if you need to prevent errors in CMS Live Preview
const dummyFilter = (string) => string;

module.exports = {
  readableDateFilter,
  markdownFilter,
  limitToFilter,
  tidyHTMLFilter,
  langFilter,
  dummyFilter,
};
